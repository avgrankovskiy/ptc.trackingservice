﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Crosscutting.Exceptions
{
    /// <summary>
    /// Исключение того, что трек-номер имеет нерусский формат
    /// </summary>
    public class TrackNumberNotRussianFormatException
        : ApplicationException
    {
        public TrackNumberNotRussianFormatException()
        {

        }

        public TrackNumberNotRussianFormatException(string message = "Трек-номер не соответствует российскому формату")
            : base(message)
        {

        }

        public TrackNumberNotRussianFormatException(Exception innerException, string message = "Трек-номер не соответствует российскому формату")
            : base(message, innerException)
        {

        }
    }
}
