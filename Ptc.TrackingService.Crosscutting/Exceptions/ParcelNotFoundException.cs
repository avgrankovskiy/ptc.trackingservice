﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Crosscutting.Exceptions
{
    /// <summary>
    /// Исключение не найденной посылки
    /// </summary>
    public class ParcelNotFoundException
        :ApplicationException
    {
        public ParcelNotFoundException()
        {

        }

        public ParcelNotFoundException(string message = "Информация об отправлении с данным трек-номером не найдена")
            : base(message)
        {

        }

        public ParcelNotFoundException(Exception innerException, string message = "Информация об отправлении с данным трек-номером не найдена")
            :base(message,innerException)
        {

        }
    }
}
