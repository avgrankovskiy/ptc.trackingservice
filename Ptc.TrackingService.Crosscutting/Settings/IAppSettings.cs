﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Crosscutting.Settings
{
    /// <summary>
    /// Интерфейс настроек
    /// </summary>
    public interface IAppSettings
    {
        /// <summary>
        /// Настройки сервиса почты
        /// </summary>
        MailServiceSettings MailServiceSettings { get; set; }
    }
}
