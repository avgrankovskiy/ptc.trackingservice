﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Crosscutting.Settings
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    public class AppSettings
        :IAppSettings
    {
        /// <summary>
        /// Настройки сервиса почты
        /// </summary>
        public MailServiceSettings MailServiceSettings { get; set; }
    }

    /// <summary>
    /// Настройки сервиса почты
    /// </summary>
    public class MailServiceSettings
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Url сервиса
        /// </summary>
        public string Url { get; set; }
    }
}
