﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarComponent } from './navbar/navbar.component';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
    imports: [CommonModule],
    declarations: [NavbarComponent, SpinnerComponent],
    exports: [NavbarComponent, SpinnerComponent],
    providers: []
})
export class CoreModule { }