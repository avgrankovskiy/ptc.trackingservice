import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//����������� ������� ���������� 
import { AppComponent } from './app.component';

//����������� ����� ������
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

//����������� ������ c ������
import { ParcelSearchModule } from './parcel-search/parcel-search.module';

import { routing, appRoutingProviders } from './app.routing';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        CommonModule,
        CoreModule,
        SharedModule,
        ParcelSearchModule,
        NgbModule.forRoot()
    ],
    providers: [
        appRoutingProviders
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }