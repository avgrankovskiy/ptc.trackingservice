import { Routes, RouterModule } from '@angular/router';

import { ParcelSearchComponent } from './parcel-search/parcel-search.component';

const appRoutes: Routes = [
    { path: '', component: ParcelSearchComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(appRoutes);