"use strict";
var router_1 = require("@angular/router");
var parcel_search_component_1 = require("./parcel-search/parcel-search.component");
var appRoutes = [
    { path: '', component: parcel_search_component_1.ParcelSearchComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map