﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoogleMapsComponent } from './google-maps/google-maps.component';
import { GoogleMapsApiService } from './google-maps/google-maps-api.service';

@NgModule({
    imports: [CommonModule],
    declarations: [GoogleMapsComponent],
    exports: [GoogleMapsComponent],
    providers: [GoogleMapsApiService]
})
export class SharedModule { }