﻿import { Injectable } from '@angular/core';

const MAPS_API_KEY = 'AIzaSyBjotkp5NEEGzAbDr4M-F0QMOPLx-vecMk';
const GEOCODING_API_KEY = 'AIzaSyCq5z2irAhZZ9bFKUsNHuYwPpXOdsD-Tfs';

const url = 'https://maps.googleapis.com/maps/api/js?key=' + MAPS_API_KEY + '&callback=initMap';

@Injectable()
export class GoogleMapsApiService {

    loadMap: Promise<any>;

    constructor() {
        this.loadMap = new Promise<any>((resolve) => {
            window['initMap'] = () => {
                resolve();
            }
            this.initApiScript();
        });
    }
     
    public initMap(): Promise<any>{
        return this.loadMap;
    }

    private initApiScript() {
        let script = document.createElement('script');

        script.src = url;
        script.text = 'text/javascript';

        document.getElementsByTagName('head')[0].appendChild(script);
    }
}