﻿import { Component, OnInit, Input } from '@angular/core';
import { GoogleMapsApiService } from './google-maps-api.service';
import GoogleMap = google.maps.Map;


@Component({
    moduleId: module.id,
    selector: 'google-maps',
    templateUrl: './google-maps.component.html',
    styleUrls: ['./google-maps.component.css']    
})
export class GoogleMapsComponent
    implements OnInit
{
    map: GoogleMap;

    private _isNeededResizing: boolean;

    @Input()
    get isNeededResizing(){
        return this._isNeededResizing;
    }

    set isNeededResizing(isNeededResizing: boolean) {
        this._isNeededResizing = isNeededResizing;
        if (this._isNeededResizing)
            this.resize();           
    }

    resize(): void {
        let center = this.map.getCenter();
        google.maps.event.trigger(this.map, 'resize');
        this.map.setCenter(center);
        this.isNeededResizing = false;
    }   

    constructor(private googleMapsApiService: GoogleMapsApiService) {

    }

    ngOnInit() {
        this.googleMapsApiService.initMap().then(() => {
            let uluru = { lat: -25.363, lng: 131.044 };
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: this.map
            });
        });
    }
}