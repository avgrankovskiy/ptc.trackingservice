"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var google_maps_api_service_1 = require("./google-maps-api.service");
var GoogleMapsComponent = (function () {
    function GoogleMapsComponent(googleMapsApiService) {
        this.googleMapsApiService = googleMapsApiService;
    }
    Object.defineProperty(GoogleMapsComponent.prototype, "isNeededResizing", {
        get: function () {
            return this._isNeededResizing;
        },
        set: function (isNeededResizing) {
            this._isNeededResizing = isNeededResizing;
            if (this._isNeededResizing)
                this.resize();
        },
        enumerable: true,
        configurable: true
    });
    GoogleMapsComponent.prototype.resize = function () {
        var center = this.map.getCenter();
        google.maps.event.trigger(this.map, 'resize');
        this.map.setCenter(center);
        this.isNeededResizing = false;
    };
    GoogleMapsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.googleMapsApiService.initMap().then(function () {
            var uluru = { lat: -25.363, lng: 131.044 };
            _this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: _this.map
            });
        });
    };
    return GoogleMapsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [])
], GoogleMapsComponent.prototype, "isNeededResizing", null);
GoogleMapsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'google-maps',
        templateUrl: './google-maps.component.html',
        styleUrls: ['./google-maps.component.css']
    }),
    __metadata("design:paramtypes", [google_maps_api_service_1.GoogleMapsApiService])
], GoogleMapsComponent);
exports.GoogleMapsComponent = GoogleMapsComponent;
//# sourceMappingURL=google-maps.component.js.map