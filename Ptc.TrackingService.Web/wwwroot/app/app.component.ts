import { Component } from '@angular/core';
import { NavbarComponent } from './core/navbar/navbar.component';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent { }