"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var parcel_service_1 = require("./../shared/parcel.service");
var ParcelInfoComponent = (function () {
    function ParcelInfoComponent() {
    }
    return ParcelInfoComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", parcel_service_1.ParcelInfo)
], ParcelInfoComponent.prototype, "parcelInfo", void 0);
ParcelInfoComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'parcel-info',
        templateUrl: './parcel-info.component.html',
        styleUrls: ['./parcel-info.component.css']
    }),
    __metadata("design:paramtypes", [])
], ParcelInfoComponent);
exports.ParcelInfoComponent = ParcelInfoComponent;
//# sourceMappingURL=parcel-info.component.js.map