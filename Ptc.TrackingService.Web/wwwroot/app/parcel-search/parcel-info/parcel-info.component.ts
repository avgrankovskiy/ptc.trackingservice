﻿import { Component, Input } from '@angular/core';
import { ParcelService, ParcelInfo } from './../shared/parcel.service';

@Component({
    moduleId: module.id,
    selector: 'parcel-info',
    templateUrl: './parcel-info.component.html',
    styleUrls: ['./parcel-info.component.css']
})
export class ParcelInfoComponent {

    @Input() parcelInfo: ParcelInfo
}