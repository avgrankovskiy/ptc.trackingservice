"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var ParcelService = (function () {
    function ParcelService(http) {
        this.http = http;
        this.parselSearchUrl = 'api/parcels';
        this.parselRouteSearchUrl = 'api/routePoints';
    }
    ParcelService.prototype.findParcel = function (trackNumber) {
        var params = new http_1.URLSearchParams();
        params.set('trackNumber', trackNumber);
        return this.http.get(this.parselSearchUrl, { search: params })
            .map(function (response) { return response.json(); })
            .catch(function (error) { return Rx_1.Observable.throw(error.json()); });
    };
    return ParcelService;
}());
ParcelService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ParcelService);
exports.ParcelService = ParcelService;
var ParcelInfo = (function () {
    function ParcelInfo(trackNumber, state, pointSource, pointDestination, daysWay, route) {
        this.trackNumber = trackNumber;
        this.state = state;
        this.pointSource = pointSource;
        this.pointDestination = pointDestination;
        this.daysWay = daysWay;
        this.route = route;
    }
    return ParcelInfo;
}());
exports.ParcelInfo = ParcelInfo;
var ParcelRoutePoint = (function () {
    function ParcelRoutePoint(postalIndex, pointDescription) {
        this.postalIndex = postalIndex;
        this.pointDescription = pointDescription;
    }
    return ParcelRoutePoint;
}());
exports.ParcelRoutePoint = ParcelRoutePoint;
var ApiResponse = (function () {
    function ApiResponse(data, message, statusCode, success) {
        this.data = data;
        this.message = message;
        this.statusCode = statusCode;
        this.success = success;
    }
    return ApiResponse;
}());
exports.ApiResponse = ApiResponse;
var ApiBaseResponse = (function () {
    function ApiBaseResponse(message, statusCode, success) {
        this.message = message;
        this.statusCode = statusCode;
        this.success = success;
    }
    return ApiBaseResponse;
}());
exports.ApiBaseResponse = ApiBaseResponse;
//# sourceMappingURL=parcel.service.js.map