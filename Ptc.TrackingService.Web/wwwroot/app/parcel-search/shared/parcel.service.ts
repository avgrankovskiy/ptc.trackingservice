﻿import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ParcelService {

    private parselSearchUrl: string = 'api/parcels';

    private parselRouteSearchUrl: string = 'api/routePoints';

    constructor(private http: Http) { }

    findParcel(trackNumber: string): Observable<ApiResponse<ParcelInfo>> {

        let params: URLSearchParams = new URLSearchParams();
        params.set('trackNumber', trackNumber);

        return this.http.get(this.parselSearchUrl, { search: params })
            .map(response => <ApiResponse<ParcelInfo>>response.json())
            .catch(error => Observable.throw(<ApiBaseResponse>error.json()));
    }
}

export class ParcelInfo {

    constructor(
        public trackNumber: string,
        public state: string,
        public pointSource: string,
        public pointDestination: string,
        public daysWay: number,
        public route: ParcelRoutePoint[]
    ) { }
}

export class ParcelRoutePoint {

    constructor(
        public postalIndex: string,
        public pointDescription: string){}
}

export class ApiResponse<T> {

    constructor(
        public data: T,
        public message: string,
        public statusCode: number,
        public success: boolean) { }
}

export class ApiBaseResponse {

    constructor(
        public message: string,
        public statusCode: number,
        public success: boolean) { }
}