"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var parcel_service_1 = require("./../shared/parcel.service");
var ParcelSearchFormComponent = (function () {
    function ParcelSearchFormComponent(parcelService) {
        this.parcelService = parcelService;
        this.searchDone = new core_1.EventEmitter();
        this.searchDoing = new core_1.EventEmitter();
    }
    ParcelSearchFormComponent.prototype.findParcel = function () {
        var _this = this;
        this.errorMessage = null;
        this.searchDoing.emit();
        this.parcelService
            .findParcel(this.trackNumber)
            .subscribe(function (response) {
            _this.searchDone.emit(response.data);
        }, function (error) { return _this.onFindParcelError(error); });
    };
    ParcelSearchFormComponent.prototype.onFindParcelError = function (error) {
        this.errorMessage = error.message;
        this.searchDone.emit(null);
    };
    return ParcelSearchFormComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], ParcelSearchFormComponent.prototype, "searchDone", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], ParcelSearchFormComponent.prototype, "searchDoing", void 0);
ParcelSearchFormComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'parcel-search-form',
        templateUrl: './parcel-search-form.component.html',
        providers: [parcel_service_1.ParcelService],
        styleUrls: ['./parcel-search-form.component.css']
    }),
    __metadata("design:paramtypes", [parcel_service_1.ParcelService])
], ParcelSearchFormComponent);
exports.ParcelSearchFormComponent = ParcelSearchFormComponent;
//# sourceMappingURL=parcel-search-form.component.js.map