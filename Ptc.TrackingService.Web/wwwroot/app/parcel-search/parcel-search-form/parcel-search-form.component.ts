﻿import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ParcelService, ParcelInfo, ApiResponse } from './../shared/parcel.service';

@Component({
    moduleId: module.id,
    selector: 'parcel-search-form',
    templateUrl: './parcel-search-form.component.html',
    providers: [ParcelService],
    styleUrls: ['./parcel-search-form.component.css']
})
export class ParcelSearchFormComponent{

    constructor(private parcelService:ParcelService){}


    private trackNumber: string
    private errorMessage: string

    @Output() searchDone: EventEmitter<ParcelInfo> = new EventEmitter<ParcelInfo>();
    @Output() searchDoing: EventEmitter<any> = new EventEmitter<any>();

    findParcel() {
        this.errorMessage = null;

        this.searchDoing.emit();
        this.parcelService
            .findParcel(this.trackNumber)
            .subscribe(
                response => {
                    this.searchDone.emit(response.data);                   
                },
                error => this.onFindParcelError(error));
    }

    onFindParcelError(error) {
        this.errorMessage = error.message;       
        this.searchDone.emit(null);                                      
    }
}