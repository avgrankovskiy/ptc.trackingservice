"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ParcelRouteMapComponent = (function () {
    function ParcelRouteMapComponent() {
        this.isCollapsed = true;
    }
    return ParcelRouteMapComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], ParcelRouteMapComponent.prototype, "parcelRoute", void 0);
ParcelRouteMapComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: "./parcel-route-map.component.html",
        selector: "parcel-route-map"
    }),
    __metadata("design:paramtypes", [])
], ParcelRouteMapComponent);
exports.ParcelRouteMapComponent = ParcelRouteMapComponent;
//# sourceMappingURL=parcel-route-map.component.js.map