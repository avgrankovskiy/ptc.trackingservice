﻿import { Component, Input } from "@angular/core";
import { ParcelRoutePoint } from "../shared/parcel.service";

@Component({
    moduleId: module.id,
    templateUrl: "./parcel-route-map.component.html",
    selector: "parcel-route-map"
})
export class ParcelRouteMapComponent {

    private isCollapsed: boolean = true;

    private trackNumber: string;

    @Input() parcelRoute: ParcelRoutePoint[]; 
}