﻿import { Component, EventEmitter } from '@angular/core';
import { ParcelService, ParcelInfo } from './shared/parcel.service';

@Component({
    moduleId: module.id,
    selector: 'parcel-search',
    templateUrl: './parcel-search.component.html',
    styleUrls: ['./parcel-search.component.css']
})
export class ParcelSearchComponent {

    private foundParcel: ParcelInfo
    private isSearchDoing: boolean = false

    onSearchDone(parcelInfo: ParcelInfo): void {
        this.isSearchDoing = false;
        this.foundParcel = parcelInfo;
    }

    onSearchDoing(): void {
        this.isSearchDoing = true;
    }
}