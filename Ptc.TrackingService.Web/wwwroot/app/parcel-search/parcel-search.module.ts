﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CoreModule } from './../core/core.module';
import { SharedModule } from './../shared/shared.module';

import { ParcelSearchComponent } from './parcel-search.component';
import { ParcelSearchFormComponent } from './parcel-search-form/parcel-search-form.component';
import { ParcelInfoComponent } from './parcel-info/parcel-info.component';
import { ParcelRouteMapComponent } from './parcel-route-map/parcel-route-map.component';

import { ParcelService } from './shared/parcel.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        CoreModule,
        SharedModule,
        NgbModule
    ],
    declarations: [
        ParcelSearchComponent,
        ParcelSearchFormComponent,
        ParcelInfoComponent,
        ParcelRouteMapComponent
    ],
    exports: [
        ParcelSearchComponent
    ],
    providers: [
        ParcelService
    ]    
})
export class ParcelSearchModule{

}