"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var core_module_1 = require("./../core/core.module");
var shared_module_1 = require("./../shared/shared.module");
var parcel_search_component_1 = require("./parcel-search.component");
var parcel_search_form_component_1 = require("./parcel-search-form/parcel-search-form.component");
var parcel_info_component_1 = require("./parcel-info/parcel-info.component");
var parcel_route_map_component_1 = require("./parcel-route-map/parcel-route-map.component");
var parcel_service_1 = require("./shared/parcel.service");
var ParcelSearchModule = (function () {
    function ParcelSearchModule() {
    }
    return ParcelSearchModule;
}());
ParcelSearchModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            core_module_1.CoreModule,
            shared_module_1.SharedModule,
            ng_bootstrap_1.NgbModule
        ],
        declarations: [
            parcel_search_component_1.ParcelSearchComponent,
            parcel_search_form_component_1.ParcelSearchFormComponent,
            parcel_info_component_1.ParcelInfoComponent,
            parcel_route_map_component_1.ParcelRouteMapComponent
        ],
        exports: [
            parcel_search_component_1.ParcelSearchComponent
        ],
        providers: [
            parcel_service_1.ParcelService
        ]
    }),
    __metadata("design:paramtypes", [])
], ParcelSearchModule);
exports.ParcelSearchModule = ParcelSearchModule;
//# sourceMappingURL=parcel-search.module.js.map