﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Web.Models.ApiResult
{
    /// <summary>
    /// Базовый результат api
    /// </summary>
    public class ApiResultBase
    {
        /// <summary>
        /// Признак успешности
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Статус код
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Конструктор результата
        /// </summary>
        /// <param name="success">Признак успеха</param>
        /// <param name="message">Сообщение</param>
        public ApiResultBase(bool success = true, int statusCode = 200)
        {
            Success = success;
            StatusCode = statusCode;
        }

        /// <summary>
        /// Конструктор результата
        /// </summary>
        /// <param name="success">Признак успеха</param>
        /// <param name="message">Сообщение</param>
        public ApiResultBase(bool success, string message, int statusCode)
        {
            Success = success;
            StatusCode = statusCode;
            Message = message;
        }
    }
}
