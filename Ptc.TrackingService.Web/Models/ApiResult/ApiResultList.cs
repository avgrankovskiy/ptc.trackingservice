﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Web.Models.ApiResult
{
    /// <summary>
    /// Результат сервиса в виде списка
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResultList<T>
        : ApiResultBase
    {
        /// <summary>
        /// Данные 
        /// </summary>
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data">Данные</param>
        /// <param name="success">Признак успешности</param>
        /// <param name="message">Сообщение</param>
        public ApiResultList(IEnumerable<T> data)
            :base()
        {
            Data = data;
        }
    }
}
