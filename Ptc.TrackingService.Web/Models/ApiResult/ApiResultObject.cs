﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Web.Models.ApiResult
{
    /// <summary>
    /// Результат запроса с объектом
    /// </summary>
    public class ApiResultObject
        :ApiResultBase
    {
        /// <summary>
        /// Объект с данными
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data">Данные</param>
        /// <param name="success">Признак успешности</param>
        /// <param name="message">Сообщение</param>
        public ApiResultObject(object data)
            :base()
        {
            Data = data;
        }
    }
}
