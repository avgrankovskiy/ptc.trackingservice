﻿using Microsoft.AspNetCore.Mvc;
using Ptc.TrackingService.Web.Models.ApiResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ptc.TrackingService.Web.Api
{
    [Route("api/[controller]")]
    public class BaseController
        :Controller
    {
        /// <summary>
        /// Создать базовый ответ api
        /// </summary>
        /// <param name="success">Признак успеха</param>
        /// <param name="statusCode">Статус код</param>
        /// <returns></returns>
        public ApiResultBase ToApiResultBase(bool success = true, int statusCode = 200)
        {
            return new ApiResultBase(success, statusCode);
        }

        /// <summary>
        /// Создать базовый ответ api
        /// </summary>
        /// <param name="success">Принзнак успеха</param>
        /// <param name="message">Сообщение</param>
        /// <param name="statusCode">Статус код</param>
        /// <returns></returns>
        public ApiResultBase ToApiResultBase(bool success, string message, int statusCode)
        {
            return new ApiResultBase(success,message, statusCode);
        }

        /// <summary>
        /// Создать результат объекта
        /// </summary>
        /// <param name="data">Данные объекта</param>
        /// <returns></returns>
        public ApiResultObject ToApiResultObject(object data)
        {
            return new ApiResultObject(data);
        }

        /// <summary>
        /// Создать результат списка
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data">Список с данными</param>
        /// <returns></returns>
        public ApiResultList<T> ToApiResultList<T>(IEnumerable<T> data)
        {
            return new ApiResultList<T>(data);
        }
    }
}
