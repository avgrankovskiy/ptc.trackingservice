﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ptc.TrackingService.Web.Models.ApiResult;
using Ptc.TrackingService.Crosscutting.Settings;
using Microsoft.Extensions.Options;
using Ptc.TrackingService.DataAccess.ExternalServices.Mail;
using Ptc.TrackingService.Crosscutting.Exceptions;
using Microsoft.AspNetCore.Http;

namespace Ptc.TrackingService.Web.Api
{
    public class ParcelsController 
        : BaseController
    {
        private readonly IMailServiceAgent _mailServiceAgent;

        public ParcelsController(IMailServiceAgent mailServiceAgent)
        {
            _mailServiceAgent = mailServiceAgent; 
        }

        // GET: api/users
        [HttpGet]
        public async Task<ApiResultBase> Get(string trackNumber)
        {
            if (string.IsNullOrWhiteSpace(trackNumber))
                return ToApiResultBase(false, "Передан пустой трек-номер", StatusCodes.Status400BadRequest);

            try
            {
                var parcel = await _mailServiceAgent.FindParcelAsync(trackNumber);

                return ToApiResultObject(parcel);
            }
            catch (ParcelNotFoundException ex)
            {
                Response.StatusCode = StatusCodes.Status400BadRequest;
                return ToApiResultBase(false, ex.Message, StatusCodes.Status400BadRequest);
            }
            catch(Exception ex)
            {
                Response.StatusCode = StatusCodes.Status500InternalServerError;
                return ToApiResultBase(false, ex.Message, StatusCodes.Status500InternalServerError);
            }
        }
    }
}
