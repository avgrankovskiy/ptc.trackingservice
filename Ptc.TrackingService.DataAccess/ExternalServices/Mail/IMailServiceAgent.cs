﻿using Ptc.TrackingService.DataAccess.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.DataAccess.ExternalServices.Mail
{
    /// <summary>
    /// Агент по работе с сервисом почты
    /// </summary>
    public interface IMailServiceAgent
    {
        /// <summary>
        /// Найти посылку асинхронно 
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        Task<ParcelInfoDto> FindParcelAsync(string trackNumber);
    }
}
