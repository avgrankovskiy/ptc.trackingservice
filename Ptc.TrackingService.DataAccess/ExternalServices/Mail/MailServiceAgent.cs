﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ptc.TrackingService.Crosscutting.Settings;
using System.Net;
using System.IO;
using Ptc.TrackingService.DataAccess.Dto;

namespace Ptc.TrackingService.DataAccess.ExternalServices.Mail
{
    /// <summary>
    /// Агент по работе с сервисом почты
    /// </summary>
    public class MailServiceAgent
        : IMailServiceAgent
    {
        private readonly IMailServiceProxy _serviceProxy;

        public MailServiceAgent(IMailServiceProxy serviceProxy)
        {
            _serviceProxy = serviceProxy;
        }

        /// <summary>
        /// Найти посылку 
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        public async Task<ParcelInfoDto> FindParcelAsync(string trackNumber)
        {
            return await _serviceProxy.FindParcelAsync(trackNumber);                             
        }
    }
}
