﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Ptc.TrackingService.Crosscutting.Settings;
using System.Net;
using System.IO;
using Ptc.TrackingService.Crosscutting.Exceptions;
using Ptc.TrackingService.DataAccess.Dto;
using System.Text.RegularExpressions;

namespace Ptc.TrackingService.DataAccess.ExternalServices.Mail
{
    /// <summary>
    /// Прокси класс для работы с сервисом почты
    /// </summary>
    public class MailServiceProxy
       : IMailServiceProxy
    {
        private readonly MailServiceSettings _mailServiceSettings;
        private readonly string _serviceMessageTemplate = @"
                <soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope""
                    xmlns:oper=""http://russianpost.org/operationhistory"" 
                    xmlns:data=""http://russianpost.org/operationhistory/data"" 
                    xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <soap:Header/>
                    <soap:Body>
                        <oper:getOperationHistory>
                            <data:OperationHistoryRequest>
                                <data:Barcode ></data:Barcode>
                                <data:MessageType>0</data:MessageType>
                                <data:Language >RUS</data:Language>
                            </data:OperationHistoryRequest>
                            <data:AuthorizationHeader soapenv:mustUnderstand = ""1"">
                                <data:login></data:login>
                                <data:password ></data:password>
                             </data:AuthorizationHeader>
                        </oper:getOperationHistory>                               
                    </soap:Body>
                </soap:Envelope>";

        public MailServiceProxy(IAppSettings settings)
        {
            _mailServiceSettings = settings.MailServiceSettings;
        }

        /// <summary>
        /// Найти посылку асинхронно
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        public async Task<ParcelInfoDto> FindParcelAsync(string trackNumber)
        {
            var message = CreateFindParcelMessage(trackNumber);
            var response = await MakeRequestAsync(message);
            return ParseFindParcelResponse(response);
        }

        /// <summary>
        /// Проверка на то, что трек-номер в российском формате
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        private bool IsRussianTrackNumber(string trackNumber)
        {
            var russianTrackNumberFormat = @"^[0-9]{1,14}$";
            var regex = new Regex(russianTrackNumberFormat);            
            return regex.IsMatch(trackNumber);
        }

        /// <summary>
        /// Создать сообщение для поиска посылки
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        private string CreateFindParcelMessage(string trackNumber)
        {
            var soapMessageXml = XDocument.Parse(_serviceMessageTemplate);

            var barcodeNode = soapMessageXml.Descendants().Where(n => n.Name.LocalName == "Barcode").SingleOrDefault();
            barcodeNode.SetValue(trackNumber);

            var loginNode = soapMessageXml.Descendants().Where(n => n.Name.LocalName == "login").SingleOrDefault();
            loginNode.SetValue(_mailServiceSettings.Login);

            var passwordNode = soapMessageXml.Descendants().Where(n => n.Name.LocalName == "password").SingleOrDefault();
            passwordNode.SetValue(_mailServiceSettings.Password);

            return soapMessageXml.ToString();
        }

        /// <summary>
        /// Делает асинхронный запрос 
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        private async Task<string> MakeRequestAsync(string message)
        {
            var webRequest = WebRequest.CreateHttp(_mailServiceSettings.Url);
            webRequest.Method = "POST"; ;

            using (var sw = new StreamWriter(webRequest.GetRequestStream()))
            {
                sw.Write(message);
            }

            string result = "";

            //Здесь можно обработать специфические ошибки HTTP
            try
            {
                var response = await webRequest.GetResponseAsync();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Парсит ответ поиска посылки
        /// </summary>
        /// <param name="response">Ответ</param>
        /// <returns></returns>
        private ParcelInfoDto ParseFindParcelResponse(string response)
        {
            if (string.IsNullOrWhiteSpace(response))
                throw new ApplicationException("Передан пустой ответ для парсинга");

            try
            {
                var xDoc = XDocument.Parse(response);

                bool responseHasData = xDoc.Descendants()
                    .Where(n => n.Name.LocalName == "OperationHistoryData").Descendants().Any();

                if (!responseHasData)
                    throw new ParcelNotFoundException();

                var parcelInfo = GetParcelInfo(xDoc);

                var trackNumber = xDoc.Descendants().Where(n => n.Name.LocalName == "Barcode").FirstOrDefault().Value;
                if(IsRussianTrackNumber(trackNumber))
                    parcelInfo.Route = GetParcelRoute(xDoc);

                return parcelInfo;
            }
            catch(ApplicationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Неверный формат сообщения", ex);
            }
        }

        /// <summary>
        /// Получить информации о посылке
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        private ParcelInfoDto GetParcelInfo(XDocument xDoc)
        {
            var operationParametersNode = xDoc.Descendants()
               .Where(n => n.Name.LocalName == "OperationParameters").ToList();

            var firstDateNode = operationParametersNode?.Descendants()
                .Where(n => n.Name.LocalName == "OperDate")
                .OrderBy(n => DateTime.Parse(n.Value))
                .FirstOrDefault();

            var firstAddressParameters = firstDateNode?.Parent.Parent.Elements()
                    .Where(n => n.Name.LocalName == "AddressParameters").FirstOrDefault();

            var pointDestinationIndexNode = firstAddressParameters?.Elements()
                .Where(n => n.Name.LocalName == "DestinationAddress")
                .Elements()
                .Where(n => n.Name.LocalName == "Index")
                .FirstOrDefault();
            var pointDestinationDescriptionNode = firstAddressParameters?.Elements()
                .Where(n => n.Name.LocalName == "DestinationAddress")
                .Elements()
                .Where(n => n.Name.LocalName == "Description")
                .FirstOrDefault();

            var pointSourceIndexNode = firstAddressParameters?.Elements()
                .Where(n => n.Name.LocalName == "OperationAddress")
                .Elements()
                .Where(n => n.Name.LocalName == "Index")
                .FirstOrDefault();
            var pointSourceDescriptionNode = firstAddressParameters?.Elements()
                .Where(n => n.Name.LocalName == "OperationAddress")
                .Elements()
                .Where(n => n.Name.LocalName == "Description")
                .FirstOrDefault();

            var firstItemParametersNode = firstDateNode?.Parent.Parent.Elements()
                .Where(n => n.Name.LocalName == "ItemParameters").FirstOrDefault();

            var barcodeNode = firstItemParametersNode?.Elements()
                .Where(n => n.Name.LocalName == "Barcode").FirstOrDefault();

            var lastDateNode = operationParametersNode?.Descendants()
                .Where(n => n.Name.LocalName == "OperDate")
                .OrderByDescending(n => DateTime.Parse(n.Value))
                .FirstOrDefault();

            var lastStateNode = lastDateNode?.Parent.Elements()
                .Where(n => n.Name.LocalName == "OperAttr")
                .Elements()
                .Where(n => n.Name.LocalName == "Name")
                .SingleOrDefault();

            var lastDate = Convert.ToDateTime(lastDateNode.Value);
            var firstDate = Convert.ToDateTime(firstDateNode.Value);

            int daysWay;
            if (lastDate == DateTime.MinValue)
                daysWay = DateTime.Now.DayOfYear - firstDate.DayOfYear;
            else
                daysWay = lastDate.DayOfYear - firstDate.DayOfYear;

            return new ParcelInfoDto()
            {
                TrackNumber = barcodeNode.Value,
                State = lastStateNode.Value,
                PointSource = $"{pointSourceIndexNode.Value} {pointSourceDescriptionNode.Value}",
                PointDestination = $"{pointDestinationIndexNode.Value} {pointDestinationDescriptionNode.Value}",
                DaysWay = daysWay
            };
        }

        /// <summary>
        /// Получить маршрут посылки
        /// </summary>
        /// <param name="xDoc"></param>
        /// <returns></returns>
        private List<RoutePointDto> GetParcelRoute(XDocument xDoc)
        {
            bool responseHasData = xDoc.Descendants()
                 .Where(n => n.Name.LocalName == "OperationHistoryData").Descendants().Any();

            if (!responseHasData)
                throw new ParcelNotFoundException();

            var points = xDoc.Descendants().Where(n => n.Name.LocalName == "OperationAddress")
                .Select(n => new RoutePointDto()
                {
                    PostalIndex = n.Elements().FirstOrDefault(e => e.Name.LocalName == "Index")?.Value,
                    PointDescription = n.Elements().FirstOrDefault(e => e.Name.LocalName == "Description")?.Value
                }).ToList();

            return points;
        }
    }
}
