﻿using Ptc.TrackingService.DataAccess.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.DataAccess.ExternalServices.Mail
{
    /// <summary>
    /// Прокси класс для работы с сервисом почты
    /// </summary>
    public interface IMailServiceProxy
    {
        /// <summary>
        /// Найти посылку асинхронно
        /// </summary>
        /// <param name="trackNumber">Трек-номер</param>
        /// <returns></returns>
        Task<ParcelInfoDto> FindParcelAsync(string trackNumber);
    }
}
