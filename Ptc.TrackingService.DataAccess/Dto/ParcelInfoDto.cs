﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ptc.TrackingService.DataAccess.Dto
{
    /// <summary>
    /// Информация о посылке
    /// </summary>
    public class ParcelInfoDto
    {
        /// <summary>
        /// Трек-номер
        /// </summary>
        public string TrackNumber { get; set; }

        /// <summary>
        /// Состояние посылки
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Пункт отправки
        /// </summary>
        public string PointSource { get; set; }

        /// <summary>
        /// Пункт назначения
        /// </summary>
        public string PointDestination { get; set; }

        /// <summary>
        /// Дней в пути
        /// </summary>
        public int DaysWay { get; set; }

        /// <summary>
        /// Маршрут посылки
        /// </summary>
        public List<RoutePointDto> Route { get; set; }
    }
}
