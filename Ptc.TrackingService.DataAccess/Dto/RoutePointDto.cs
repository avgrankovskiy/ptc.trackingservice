﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ptc.TrackingService.DataAccess.Dto
{
    /// <summary>
    /// DTO с частью маршрута посылки
    /// </summary>
    public class RoutePointDto
    {
        /// <summary>
        /// Почтовый индекс
        /// </summary>
        public string PostalIndex { get; set; }

        /// <summary>
        /// Описание точки маршрута
        /// </summary>
        public string PointDescription { get; set; }
    }
}
